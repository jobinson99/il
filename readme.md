# 友码 Il is markup language  一种标记式文本语言，特点是 规则统一精炼，容易扩展，容易掌握，视觉舒服。

![标志](/docs/images/il-logo.png)

> ease and extensible

**State** : Syntax style stable, with some flaw to fixed.


## #2 特点

+ 区块全部无行首缩进
+ Extensible：
  + inline and block
  + easily to add extension
+ Unified Rule：
  + 起始和终点符号一致
  + 自定义行内样式和链接规则统一
  + 可方便添加类名和识别名
- Comfortable in sense.


## #2 Stable rules

inline markup
```
[* bold]
[/ italic]
[_ underline]
[+ strike through]
[^ superscript]
[~ subscript]
[= code]
```

link
```
[>link-address-http://www.bian.ga/ link-name-bian.ga]
[!image-dir/image.png image-name]
[#in-page-id-name subject-name]
[<in-page-ref position-in-place]
[:class name :class name content]
```

block

```
#1 title 1
#2 title 2
#1+   ordered title 1


=== 分隔线 hr
Any = or as many = as you wish
========================

- list
+ order list
-+
-[  multiline list start
-]  multiline list end

> blockquote
>[ multiline blockquote start
>] multiline blockquote end


=[ code block, preformated, language start
=] code block, preformated, language end

Any = or as many = as you wish
=============[ javascript hightlight pretty init
code block
===]


comment
=[ comment
=]

meta
=[ meta
=]

table
=[ table
=]

math
=[ math
=]

vector diagram
=[ graphviz
=]

```

效果 见 截图，实测效果可参见 test文件夹

![screenshot](/docs/images/screen1.png)


## #2 Rules in Development

参见 [设计草案](/docs/2018-06-25-友码文设计大体定案.md)

尚需解决几个小问题：

- 自定义类起首要不要特定标记？用 `:`
- 起首符号后，要不要加空格？
- 中间间隔符用什么？目前用空格
- 类需要设置一个起首标识符？用 `:`

## #2 TODO list

- [ ] 实现 nodejs 编译器
- [x] emacs il-mode
- [ ] emacs snippets
- [x] kwrite [kde syntax highlighting framework](https://github.com/KDE/syntax-highlighting)，20180627初步搞定。
- [x] for mobile user: jota highlighting: 20180625 爬山登顶后，小坐一会，搞定。
- [ ] vscode highlighting
- [ ] atom highlighting
- [ ] vim highlighting
- [ ] codemirror高亮方案：在src/mode/里
- [ ] 制造一个编辑器，使其能把各种媒体内容放入，保存时归档，打开是解开归档。从而形成一个编辑器。
- [ ] 拆分文档为中英文模式，而非现在的混杂模式。

> Note: emacs和kate的reg引擎都有瑕疵，大体不差，但还需要进一步适配，以精确表达规则。


## #2 Quick Start

### #3 Syntax Highlight only

> TODO: publish the mode to the corresponding platform.

As the following mode is not published to the target platform, we need to add the mode by hand.

根据所使用平台，调用derivation里的相应插件即可。

#### #4 il-mode in Emacs

Make a symbolic link or move the file to the target directory.

``` shell
ln -s /path/to/derivation/emacs-mode/il-mode.el ~/.emacs.d/plugins
ln -s /path/to/derivation/snippet ~/.emacs.d/snippet/il-mode
```
then add the following lines to the file init.el in order to autoload the mode:
```
;; autoload il-mode
(autoload 'il-mode "il-mode")
(add-to-list 'auto-mode-alist '("\\.il\\'" . il-mode) t)
```

#### #4 kate/kwrite Syntax Highlight

Copy `/path/to/derivation/kde-il/il.xml` or make a symbolic link to the target directory.

``` shell
ln -s /path/to/derivation/kde-il/il.xml ~/.local/share/org.kde.syntax-highlighting/syntax/il.xml

```
#### #4 jota Syntax Highlight

Copy `/derivation/jota/il.conf` to the directory `.jota/keyword/user/` in phone's sdcard。


### #3 output the il file to html：

Attention: can not work at present.

```
npm instal -g il-mode
il-mode -i inputfile -o outputfile
```

## #2 How to Contribute

### #3 File Structure：

- lib: contains all source code.
- docs: contains all document including design draft.
- derivation: contains all derivation for many platforms, such as emacs vim kate vscode github-atom
- test: contains all test files
- public: empty, for docs to deploy as static web pages.

## #2 History

+ 20141105 Design begins
+ 20180617 端午前夕，初步完成emacs il-mode
+ 20180622 规则初步定案
+ 20180625 调整规则：行内为单个标识符且带始终标识，首写。连接统一设置，可扩展区块换标识符。完成手机编辑器jota的高亮适配。
+ 20180626 完成kate/kwrite高亮移植。
+ 20200430 设定行内自定义标识为 [:classname] 

